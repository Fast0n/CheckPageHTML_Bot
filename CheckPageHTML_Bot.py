#!/bin/python3.6
import os
import sys
import telepot
import requests
from time import sleep
from settings import token, refresh_time, start_msg,\
    stop_msg, client_file, global_url
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton

def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    chat_id = msg['chat']['id']
    command_input = msg['text']

    if command_input == '/start':
        # Check if already registred
        if registerClientID(chat_id):
            bot.sendMessage(chat_id, start_msg)

    if command_input == '/stop':
        bot.sendMessage(chat_id, stop_msg, parse_mode='Markdown')
        removeClientID(chat_id)

def update():
    while 1:
        try:
            msg = "Sito aggiornato"
            sendToAll(msg)
            sleep(refresh_time)
        except Exception as e:
            if e == KeyboardInterrupt:
                sys.exit()

def registerClientID(chat_id):
    try:
        f = open(client_file, "r+")
    except IOError:
        f = open(client_file, "w")
        f.write(str(chat_id) + '\n')
        f.close()
        return True

    insert = 1

    for client in f:
        if client.replace('\n', '') == str(chat_id):
            insert = 0

    if insert:
        f.write(str(chat_id) + '\n')
        f.close()
        return True

def sendToAll(msg):

    try:
        f = open(client_file, "r")
    except IOError:
        return
    for client in f.readlines():
        try:
            # Se non esiste il file 'page.html' lo crea
            if not os.path.isfile("./page.html"):
                # URL di partenza
                url = global_url
                r = requests.get(url)
                # Crea un file clone del sito web
                open('page.html', 'wb').write(r.content)
                sleep(10)

            # URL di confronto
            url1 = global_url
            r1 = requests.get(url1)
            # Crea un file clone del sito web
            open('page_2.html', 'wb').write(r1.content)
            sleep(10)

            # Indirizzi dei vari file
            file1 = "page.html"
            file2 = "page_2.html"
            sleep(10)
            # Confronta il primo file con il secondo
            test = os.popen("diff -c " + file1 + " " + file2).read()
            test = test.split("\n")
            if '\n'.join(test) != '':
                keyboard = InlineKeyboardMarkup(inline_keyboard=[
                            [InlineKeyboardButton(text='Controlla sito', url=global_url)],
                        ])

                bot.sendMessage(client, "Log:\n `" + '\n'.join(test[2:]).replace("! ", "- ").replace("< ", "+ ") + "`\n" + msg, parse_mode="Markdown", reply_markup=keyboard)
                sleep(2)
                os.system("rm -rf " + file1)

        except:
            removeClientID(int(client))
            continue
    f.close()

def removeClientID(chat_id):
    try:
        f = open(client_file, "r+")
    except IOError:
        return

    tmp = ''

    for client in f:
        if client.replace('\n', '') != str(chat_id):
            tmp = tmp + str(client)

    f.close()

    f = open(client_file, "w")
    for client in tmp:
        f.write(client)

    f.close()

# Main
print("Starting CheckPageHTML_Bot...")

# PID file
pid = str(os.getpid())
pidfile = "/tmp/CheckPageHTML_Bot.pid"

# Check if PID exist
if os.path.isfile(pidfile):
    print("%s already exists, exiting!" % pidfile)
    sys.exit()

# Create PID file
with open(pidfile, 'w') as f:
    f.write(pid)
    f.close()

# Start working
try:
    bot = telepot.Bot(token)
    bot.message_loop(on_chat_message)
    update()
finally:
    os.unlink(pidfile)